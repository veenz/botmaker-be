import json

def get_config() -> dict:
    with open('config.json', 'rb') as target:
        config = json.load(target)  
    return config